\chapter{Vorstellung des Modellfahrzeuges und Stand der Technik}

\section{Vorstellung des Modellfahrzeuges}
Dieses Projekt wird am GIGATRONIK Connected Car (GTCC) umgesetzt. Das GTCC ist ein Modellauto mit einem Maßstab von 1:8 (siehe Abbildung \ref{img:MFZG}), an dem Studenten verschiedene Funktionen entwickeln. Das Ziel des Modellfahrzeugprojektes ist es, nach dem sogenannten SLAM (Simultaneous Localization and Mapping) Verfahren eine Karte der Umgebung mit Hilfe eines Laserscanners zu generieren und anschließend auf dieser Karte nach Parkplätzen zu suchen. Durch eine Bahnplanung soll dann zu einem beliebigen Parkplatz von einer beliebigen Position aus geplant und mit dem Bahnfolgeregler gefahren werden. \newline
\begin{figure}[h]
\begin{center}
               \includegraphics[height = 180pt]{GTCC.jpeg}
               \caption{Das Modellfahrzeug GTCC}
               \label{img:MFZG}
\end{center}
\end{figure}
Im Folgenden werden die Hard- und Software des Fahrzeugs kurz vorgestellt.

\subsection{Hardware am Modellfahrzeug}

\begin{description}
\item[Arduino Due] Das Arduino Due ist ein Mikrocontroller Board basierend auf einem 32-bit SAM3X8E ARM Cortex-M3 von Atmel. Es übernimmt die Low-Level Aufgaben der Geschwindigkeits- und Lenkwinkelregelung, sowie die Verarbeitung von Sensorsignalen.
\item[Odroid-XU4] Der Odroid übernimmt die High-Level Aufgaben, wie Trajektorienfolgeregelung oder Bahnplanung. Er kommuniziert via SPI mit dem Arduino und sendet so beispielsweise Geschwindigkeits- und Lenkwinkelsollwerte.
\item[Platinium Brushless Motor] Der Platinium Brushless Motor sorgt in Verbindung mit dem Speedstar Brushless Crawler Fahrtenregler für eine genaue Geschwindigkeitsregelung.
\item[Absima Servo S90MH] Der Servomotor stellt den Lenkwinkel ein und wird über Pulsweiten-Modulation gesteuert. Das heißt, dass die Breite des gesendeten Pulses die Position des Servomotors bestimmt.
\item[IMU MPU-6050] Die Inertiale Messeinheit (IMU) besitzt einen Gyro- und Beschleunigungssensor und dient dazu, die Gierrate des Fahrzeuges zu messen. Sie kann über $I^{2}C$ vom Arduino Board ausgelesen werden.
\item[Inkrementalgeber GP1A30] Mit dem optischen Encoder im Inkrementalgeber lassen sich die Geschwindigkeit und Fahrtrichtung des Fahrzeuges berechnen.
\item[RPLidar A2] Der RPLidar A2 ist ein 2D $360^\circ$ Laserscanner. Mit diesem wird in Echtzeit eine Karte erstellt und das Fahrzeug auf dieser lokalisiert. Dies geschieht nach dem SLAM Verfahren. 
\item[Kinect Kamera] Die Microsoft Kinect Kamera dient mit ihren Tiefen- und Bilddaten für das Erstellen von 3D Karten und zum Erkennen von Objekten, wie zum Beispiel Straßenschildern.
\end{description}
\subsection{Software auf dem Modellfahrzeug}
Die Funktionen für das GTCC werden in ROS (Robot Operating System) entwickelt. ROS ist ein System, in dem man Funktionen für Roboter aller Art entwickeln kann. Diese Funktionen können auf verschiedenen Geräten parallel laufen und miteinander kommunizieren. 
In der ROS Struktur dieses Projektes existieren bereits folgende Knoten:
\begin{description}
\item[Bahnplanung] Der Bahnplanungsknoten berechnet von der Ist-Position eine Bahn zu der Soll-Position. Diese berechnete Bahn wird als n x 4 Matrix ausgegeben. In den vier Spalten stehen die x- und y-Koordinaten, die Soll-Orientierung $\theta_{\mathrm{soll}}$ und die Fahrtrichtung.
\item[Trajektoriengenerierung] Der Trajektoriengenerierungsknoten erstellt aus einer Bahn eine Trajektorie. Hierfür erhält er von der Bahnplanung eine Bahn.
\item[Ego Motion] Der Ego Motion Knoten übernimmt die Eigenbewegungsschätzung des GTCC. Er berechnet aus den Raddrehzahlsensoren und der Gierrate $\dot{\psi}$ mit Hilfe eines Kalman Filters die Position im globalen Koordinatensystem. \newline
Hierfür wird die Fahrzeugkinematik mit dem kinematischen Einspurmodell (siehe Abbildung \ref{img:Einspurmodell}) nach \cite{KESM} vereinfacht. Bei diesem Einspurmodell werden als Vereinfachung die Räder einer Achse zusammengefasst und der Schwerpunkt (SP) auf Fahrbahnhöhe gelegt. Des Weiteren werden alle Querkräfte vernachlässigt und auch der Schwimmwinkel zu Null angenommen. Bestimmt wird die Bewegung des Fahrzeuges dann vollständig durch die Geschwindigkeit $v$, den Radstand $L_{\mathrm{R}}$, den Lenkwinkel $\delta$, die Gierrate $\dot{\psi}$ und die Fahrzeugorientierung $\theta$ und wird als Drehung um den Momentanpol (MP) aufgefasst. Dies berechnet sich wie folgt:
\begin{equation}
\dot{\psi} = \frac{v}{L_{\mathrm{R}}} \cdot \tan\delta 
\end{equation}
\begin{equation}
\theta = \int \dot{\psi} \, dt 
\end{equation}
\begin{equation}
\begin{bmatrix}
x_{\mathrm{ist}} \\
y_{\mathrm{ist}}
\end{bmatrix}
=
\int
\begin{bmatrix}
v \cdot \cos\theta \\
v \cdot \sin\theta 
\end{bmatrix}
dt
\end{equation}
\begin{figure}[h]
\begin{center}
               \includegraphics[height = 200pt]{Einspurmodell.pdf}
               \caption{Einspurmodell}
               \label{img:Einspurmodell}
\end{center}
\end{figure}



\item[Trajektorienfolgeregler] Der Trajektorienfolgeregler regelt Lenkwinkel und Geschwindigkeit, um die vorgegebene Trajektorie abzufahren. Hierfür erhält er von der Trajektoriengenerierung die Trajektorie und vom Ego Motion Knoten die Ist-Position des Fahrzeuges. 
\end{description}

\subsection{Limitierungen des Modellfahrzeuges}

Durch die verwendete Hardware auf dem GTCC ergeben sich diverse Limitierungen. So kann der Lenkservomotor lediglich einen Lenkwinkel von $25^\circ$ einstellen, obwohl achsenseitig ein Lenkeinschlag von $35^\circ$ möglich wäre. \newline
Auch fehlt der Lenkvorrichtung eine Rückführung des Lenkwinkels,  sodass für die Lenkung lediglich eine Steuerung, aber keine Regelung möglich ist. \newline
Außerdem kann das Fahrzeug aufgrund der ungleichen Spur- und Sturzeinstellungen der Räder an der Hinterachse und einem leichten Spiel in der Lenkung nicht perfekt geradeaus fahren.

\subsection{Motivation für einen Bahnfolgeregler am Modellfahrzeug}

Auf dem Fahrzeug existiert und läuft bereits ein Trajektorienfolgeregler. Dieser wurde entwickelt, um die Bahnen aus der Bahnplanung mit den Trajektorien aus der Trajektorienplanung zu vergleichen, da dieser sowohl mit Trajektorien, als auch mit umgewandelten Bahnen umgehen kann. Demzufolge sieht die ROS-Struktur momentan wie folgt aus:
\begin{figure}[H]
\begin{center}
               \includegraphics[height = 70pt]{ROS1.pdf}
               \caption{ROS-Knoten mit Trajektorienfolgereger}
\end{center}
\end{figure}
Da die Bahnplanung in der Entwicklung weit fortgeschritten ist und man den Schritt der Trajektoriengenerierung überspringen möchte, soll nun ein Bahnfolgeregler entwickelt werden. Die Trajektorienplanung wurde aufgrund nicht zufriedenstellender Ergebnisse nicht weiter verfolgt. Dadurch soll die ROS-Struktur wie folgt aussehen:\begin{figure}[H]
\begin{center}
               \includegraphics[height = 50pt]{ROS2.pdf}
               \caption{ROS-Knoten mit Bahnfolgeregler}
\end{center}
\end{figure}

\section{Stand der Technik}
Die Herausforderung das gesamte Parkmanöver mit nur einem Regler abzudecken liegt in den beiden Phasen dessen. Zuerst gibt es die Fahrt zum Parkplatz hin, die sogenannte Transferfahrt. Diese enthält ausschließlich Abschnitte, die vorwärts und mit höherer Geschwindigkeit gefahren werden. Hierbei ist ein gleichmäßiger Bewegungsfluss wichtig. Hat man den Parkplatz erreicht, erfolgt die Phase des Einparkens. Das Einparkmanöver fordert andererseits präzises Lenken, das Einhalten der richtigen Orientierung und kein Überschießen über die finale Zielpose, um diese präzise zu erreichen. Sonst besteht aufgrund der Nichtholonomität des Fahrzeuges eine Gefahr der Kollision mit Hindernissen, denn es kann nicht unabhängig voneinander Position und Orientierung verändern.\newline
Im Folgenden werden aktuell verwendete Systeme und anschließend häufig verwendete Regelansätze in der Bahnfolge vorgestellt.

\subsection{Aktuell verwendete Systeme}
Die in der Einleitung erwähnten vollautomatisierten Systeme sind noch nicht serienreif, dennoch bieten die meisten heutigen Fahrzeuge beim Einparken ausgereifte Assistenzsysteme an. \newline
In den aktuellen Fahrzeugen befinden sich Systeme, die entweder rechts oder links vom Fahrzeug bei langsamen Geschwindigkeiten nach einer Parklücke suchen. Ist eine Parklücke gefunden, muss der Fahrer im Fahrzeug den Befehl zum Einparken geben und dabei konstant das System überwachen \cite{GolfParken}.
Bei manchen Modellen kann mittlerweile der Befehl zum Einparken und die Überwachung per Handy oder den Fahrzeugschlüssel außerhalb des Fahrzeuges übernommen werden, sodass das Fahrzeug in engere Parklücken einparken kann, da der Fahrer nicht mehr aussteigen muss \cite{AudiParken}.


\subsection{Geometrische Regelansätze} \label{sec:geo}
Geometrische Regelansätze verwenden elementare geometrische Beziehungen, um Lenkwinkelvorgaben zu berechnen.
\subsubsection{Follow The Carrot Algorithmus}
Eine der einfachsten Umsetzungen der Bahnfolgeregelung ist der Follow The Carrot Algorithmus und wird in \cite{carrot} vorgestellt. Dessen Geometrie ist in Abbildung \ref{img:FTC} dargestellt. Dabei wird zuerst der Soll-Punkt gesucht. Das ist derjenige Bahnpunkt, der am weitesten von der Ist-Position entfernt ist, aber immer noch innerhalb der Look-Ahead Distanz liegt. Danach wird der Orientierungsfehler $\alpha$ zwischen der Fahrzeugorientierung und dem Soll-Punkt berechnet. Dieser Orientierungsfehler wird schließlich mit einem P-Regler minimiert. \newline
\begin{figure}[htb]
\begin{center}
               \includegraphics[height=180pt]{FTC.pdf}
               \caption{Geometrie des Follow The Carrot Algorithmus}
               \label{img:FTC}
\end{center}
\end{figure}

Wegen der einfachen Geometrie und dem simplen Regleransatz ergeben sich große Nachteile bei der Verwendung des Follow The Carrot Algorithmus. Wird die Look-Ahead Distanz zu klein gewählt, so oszilliert das Fahrzeug um den Pfad herum, da es zu spät nachfolgende Bahnpunkte beachtet. Bei einer zu großen Look-Ahead Distanz werden Kurven geschnitten, da zu früh Punkte angefahren werden, die noch weit entfernt liegen. 
\begin{figure}[htb]
\begin{center}
               \includegraphics[height=180pt]{FTCLAD.pdf}
               \caption{Auswirkungen der Look-Ahead Distanz auf die Bahnfolge}
               \label{img:FTCLAD}
\end{center}
\end{figure}

Aufgrund dieser Probleme ist dieser Algorithmus nicht für präzises Bahnfolgen geeignet und findet keine Anwendung.


\subsubsection{Pure Pursuit Algorithmus} \label{sec:pp}
Der Pure Pursuit Algorithmus ist ähnlich dem Follow The Carrot Algorithmus. Hierbei wird ebenfalls innerhalb einer vorgegebenen Look-Ahead Distanz ein Zielpunkt gesucht und anschließend die Orientierungsdifferenz zwischen dem Zielpunkt und dem Fahrzeug berechnet. Anstatt diesen Fehler mit einem P-Regler zu minimieren, legt der Pure Pursuit Algorithmus einen Kreis durch die Ist-Position des Fahrzeuges und den Zielpunkt. Daraus ergibt sich der erforderliche Lenkwinkel, um zum Zielpunkt zu gelangen. Diese Berechnung findet in jedem Zeitschritt statt und läuft nach \cite{PurePursuit1} in folgenden Schritten ab: 

\begin{figure}[H]
\begin{center}
               \includegraphics[height = 200pt]{PurePursuitGeometrie.pdf}
               \caption{Geometrie des Pure Pursuit Algorithmus; in Anlehnung an \cite{carrot, PurePursuit1}, eigene Darstellung}
               \label{img:Pure Pursuit Geometrie}
\end{center}
\end{figure}

\begin{enumerate}
\item \textbf{Bestimme die Ist-Position des Fahrzeuges} \newline
\item \textbf{Finde den anzufahrenden Punkt auf der Bahn} \newline 
Um den nächsten Soll-Punkt zu finden, wird derjenige Bahnpunkt gesucht, der am weitesten vom Fahrzeug entfernt ist, aber noch innerhalb der Look-Ahead Distanz liegt.
\item \textbf{Berechne den benötigten Lenkwinkel und sende diesen an das Fahrzeug} \newline
Um den Lenkwinkel berechnen zu können, benötigt man zuerst die Krümmung $\kappa$ des zu fahrenden Bogens. Diese ist durch die Orientierungsdifferenz $\alpha$ zwischen der Fahrzeugorientierung $\theta$ und dem Zielpunkt sowie dem Abstand $D$ zwischen Soll- und Ist-Punkt folgendermaßen bestimmt \cite{PPFormeln}:
\begin{equation}
\alpha=\arctan(\frac{y_\mathrm{soll} - y_\mathrm{ist}}{x_\mathrm{soll} - x_\mathrm{ist}}) - \theta
\end{equation}
\begin{equation}
\kappa=\frac{2\cdot\sin\alpha}{D}
\end{equation}
\begin{equation}
\delta=\arctan (L_{\mathrm{R}}\cdot\kappa)
\end{equation}
Dabei ist die Fahrzeugorientierung $\theta$ als der Winkel zwischen der x-Achse des Fahrzeugkoordinatensystems und der x-Achse des globalen Koordinatensystems bestimmt.
\item \textbf{Aktualisiere die Ist-Position} \newline
Beziehe die aktualisierte Ist-Position des Fahrzeuges und fahre bei Schritt 2 fort.
\end{enumerate}


In seiner Grundform weist der Pure Pursuit Algorithmus ähnliche Schwächen wie der Follow The Carrot Algorithmus auf. Dabei sind aber sowohl das Oszillieren um den Pfad, als auch das Schneiden von Kurven durch die andere Geometrie wesentlich weniger präsent. Allerdings beachtet der Pure Pursuit Algorithmus ebenfalls nicht die Orientierung des Zielpunktes, sondern lediglich dessen x- und y- Koordinaten. \newline
Trotzdem findet der Algorithmus sehr häufig Anwendung. Dies liegt an seiner vergleichsweise leichten Implementierung und dennoch guten Bahnfolgeeigenschaften. In \cite{PurePursuit1} wird er beispielsweise zur Umsetzung von normalen Fahrszenarien verwendet. Für Einparkszenarien findet er in \cite{PPParking} Anwendung. 
In \cite{AutoSteer} wurden verschiedene Bahnfolgealgorithmen verglichen und der Pure Pursuit Algorithmus als gut geeignet für Fahrten mit niedriger Geschwindigkeit herausgearbeitet.

\subsubsection{Vector Pursuit Algorithmus}
Vector Pursuit ist ein geometrischer Regelansatz und verwendet die Schraubentheorie, die erstmals 1876 von Sir Robert Stawell Ball veröffentlicht wurde \cite{Schrauben}. Mit dieser Theorie lässt sich die Bewegung eines starren Körpers darstellen. \newline
Im Gegensatz zu den beiden bereits vorgestellten geometrischen Ansätzen, beachtet der Vector Pursuit Algorithmus sowohl die Position eines Punktes, als auch die gewünschte Orientierung an diesem. Der Vector Pursuit Algorithmus sucht sich, wie auch die anderen geometrischen Ansätze, einen Zielpunkt innerhalb einer Look-Ahead Distanz und berechnet dann mit Hilfe von zwei Schraubbewegungen die Bewegung des Fahrzeuges zu diesem Zielpunkt. Die erste Schraube beschreibt dabei die Bewegung des Fahrzeuges von seiner Ist-Position zur Soll-Position und die zweite Schraube berechnet die benötigte Drehung, um den Zielpunkt in der gewünschten Orientierung zu erreichen. Im nächsten Schritt werden die Bewegungen der beiden Schrauben zusammengelegt und daraus der gewünschte Lenkwinkel berechnet \cite{carrot}. \newline
Verwendet wird der Algorithmus zum Beispiel in \cite{VectorPursuit} für die Bahnregelung an einem Unterwasserfahrzeug. 
Die Tests von \cite{VectorPursuit1} zeigen etwas bessere, aber vergleichbare Ergebnisse zu einem gut parametrierten Pure Pursuit Algorithmus. Besonders gut konnte der Vector Pursuit Algorithmus in diesen Tests mit Sprüngen in der Bahn oder Ist-Position umgehen und hat dort schnell wieder auf die Bahn zurückgefunden.


\subsection{Modellbasierte Regelansätze} \label{sec:mod}
Modellbasierte Ansätze verwenden ein Modell des zu regelnden Fahrzeuges, um Lenkwinkelvorgaben zu berechnen. Die Performanz ist dabei im Wesentlichen abhängig von der Güte des Modells.
\subsubsection{Model Predicitive Control}
Bei der Model Predictive Control Regelung werden nach \cite{MPC} anhand eines Fahrzeugmodells und vorhergesagten zukünftigen Eingängen zukünftige Zustände des Fahrzeuges geschätzt. Anschließend kann durch eine Kostenfunktion der optimale Eingang für das System bestimmt werden (siehe Abbildung \ref{img:MPC}).
\begin{figure}[htb]
\begin{center}
               \includegraphics[width=\textwidth]{MPC2.pdf}
               \caption{Regelkreisstuktur der Model Predicitive Control \cite{MPCRK}, eigene Darstellung}
               \label{img:MPC}
\end{center}
\end{figure}
 \newline 
Dem Algorithmus wird dafür ein Modell des Fahrzeuges vorgegeben. Ferner können Beschränkungen aller Art, wie zum Beispiel ein maximaler Lenkwinkel oder eine maximale Querbeschleunigung, definiert werden. Durch die Modellkenntnis betrachtet der Algorithmus zukünftige Zustände, um die optimalen Eingangsgrößen für das Fahrzeug zu bestimmen. Dabei ist durch die Größe des Horizontes $H$ bestimmt, wie viele Zeitschritte der Algorithmus in die Zukunft schauen soll. Je größer dieser Wert ist, umso größer ist auch der Rechenaufwand. In Abbildung \ref{img:MPC2} ist diese Vorhersage bei einer Bahnfolge dargestellt.\newline
\begin{figure}[htb]
\begin{center}
               \includegraphics[width=\textwidth]{./plots/MPC.eps}
               \caption{Darstellung der Model Predictive Control \cite{MPCBild}, eigene Darstellung}
               \label{img:MPC2}
\end{center}
\end{figure}
Durch eine Optimierungsfunktion wird entschieden, welches der vorhergesagten Szenarien den besten Ausgang liefert. Von diesem Szenario wird dann die Eingangsgröße k an das Fahrzeug gesendet, sodass Richtung Position k+1 gefahren wird. Unabhängig von der Größe des Horizontes $H$ werden die restlichen vorhergesagten Eingangsgrößen verworfen. Anschließend wird die neue Ist-Position rückgeführt und die Berechnung wieder von vorne begonnen. \newline
Dieser Regelansatz findet zum Beispiel in \cite{MPCNormal} Anwendung bei der Bahnfolge von normalen Fahrszenarien und in \cite{MPCParking} bei Einparkvorgängen. In \cite{MPCNormal} liefert dieser Algorithmus gute Ergebnisse. Es ist vor allem hervorzuheben, dass der Algorithmus in dieser Umsetzung ein Ausbrechen bei Kurvenfahrten durch die Modellkenntnis verhindert, was mit geometrischen Ansätzen bei höheren Geschwindigkeiten nicht gegeben ist. \newline
Ein Nachteil von diesem modellbasierten Ansatz sind die vielen benötigten Parameter, die für die Vorhersage des Modellzustandes benötigt werden. Außerdem ist der Bedarf an Rechenleistung sehr hoch, sodass bei limitierter Hardware eine Anwendung in Echtzeit nicht garantiert ist.









