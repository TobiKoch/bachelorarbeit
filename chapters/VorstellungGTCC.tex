\chapter{Vorstellung des Modellfahrzeuges}

Dieses Projekt wird am GIGATRONIK Connected Car (GTCC) umgesetzt. Das GTCC ist ein Modellauto mit einem Maßstab von 1:8, an dem Studenten verschiedene Funktionen entwickeln. Das Ziel dieses Projektes ist es ein SLAM Verfahren mit Hilfe eines Laserscanners zu implementieren und anschließend auf der generierten Karte nach Parkplätzen zu suchen. Durch eine Bahnplanung soll dann zu einem beliebigen Parkplatz von einer beliebigen Position geplant und mit dem Bahnfolgeregler gefahren werden. \newline
Im Folgenden werden die Hard- und Software auf dem Fahrzeug kurz vorgestellt.

\section{Hardware am Modellfahrzeug}

\begin{description}
\item[Arduino Due] Das Arduino Due ist ein Mikrocontroller Board basierend auf dem 32-bit SAM3X8E ARM Cortex-M3 von Atmel. Es übernimmt die Low-Level Aufgaben der Geschwindigkeits- und Lenkwinkelregelung, sowie die Verarbeitung von Sensorsignalen.
\item[Odroid-XU4] Der Odroid übernimmt die High-Level Aufgaben, wie Trajektorienfolgeregelung oder Bahnplanung. Er kommuniziert via SPI mit dem Arduino und sendet so beispielsweise Geschwindigkeits- und Lenkwinkelsollwerte.
\item[Platinium Brushless Motor] Der Platinium Brushless Motor sorgt in Verbindung mit dem Speedstar Brushless Crawler Fahrtenregler für eine genaue Geschwindigkeitsregelung.
\item[Absima Servo S90MH] Der Servomotor stellt den Lenkwinkel und wird über ein PWM-Signal gesteuert. 
\item[IMU MPU-6050] Die Inertiale Messeinheit (IMU) besitzt eine Gyro- und Beschleunigungssensor und wird verwendet, um die Gierrate des Fahrzeuges zu messen. Sie kann über $I^{2}C$ vom Arduino Board ausgelesen werden.
\item[Inkrementalgeber GP1A30] Mit dem optischen Encoder im Inkrementalgeber lassen sich die Geschwindigkeit und Fahrtrichtung des Fahrzeuges berechnen.
\item[RPLidar A2] Der RPLidar A2 ist ein 2D $360^\circ$ Laserscanner. Mit diesem wird eine Karte erstellt und das Fahrzeug auf dieser lokalisiert. Dies geschieht nach dem sogenannten SLAM (Simultaneous Localization and Mapping) Verfahren. 
\item[Kinect Kamera] Die Microsoft Kinect Kamera dient mit ihren Tiefen- und Bilddaten für das Erstellen von 3D Karten und zum Erkennen von Objekten, wie zum Beispiel Straßenschildern.
\end{description}
\section{Software auf dem Modellfahrzeug}
Die Funktionen für das GTCC werden in ROS (Robot Operating System) entwickelt. ROS ist ein System, in dem man Funktionen für Roboter aller Art entwickeln kann. Diese Funktionen können auf verschiedenen Geräten parallel laufen und miteinander kommunizieren. 
In der ROS Struktur dieses Projektes existieren bereits folgende Knoten:
\begin{description}
\item[Bahnplanung] Der Bahnplanungsknoten berechnet aus der Soll- und Ist-Position eine Bahn unter Beachtung der Nicht Holonomität des Fahrzeuges.
\item[Trajektoriengenerierung] Der Trajektoriengenerierungsknoten erstellt aus einer Bahn eine Trajektorie. Hierfür erhält er von der Bahnplanung eine Bahn mit x und y Koordinaten und der Orientierung $\theta$.
\item[Ego Motion] Der Ego Motion Knoten ist für die Eigenbewegungsschätzung zuständig. Er berechnet aus den Raddrehzahlsensoren und der Gierrate $\dot{\Psi}$ mit Hilfe eines Kalman Filters die Position im globalen Koordinatensystem. \newline
Hierfür wird die Fahrzeugkinematik mit dem kinematischen Einspurmodell (siehe Abbildung \ref{img:Einspurmodell}) nach \cite{KESM} vereinfacht. Bei diesem Einspurmodell werden als Vereinfachung die Räder einer Achse zusammengefasst und der Schwerpunkt (SP) auf Fahrbahnhöhe gelegt. Des Weiteren werden alle Querkräfte vernachlässigt und auch der Schwimmwinkel zu Null angenommen. Bestimmt wird die Bewegung des Fahrzeuges dann vollständig durch die Geschwindigkeit $v$ und die Gierrate $\dot{\Psi}$. Diese berechnet sich wie folgt:
\begin{equation}
\dot{\Psi} = \frac{v}{L_{R}} * \tan\delta 
\end{equation}
Mit dieser Gierrate dreht sich das Fahrzeug um den Momentanpol (MP).   
\begin{figure}[h]
\begin{center}
               \includegraphics[height = 200pt]{Einspurmodell.pdf}
               \caption{Einspurmodell}
               \label{img:Einspurmodell}
\end{center}
\end{figure}



\item[Trajektorienfolgeregler] Der Trajektorienfolgeregler regelt Lenkwinkel und Geschwindigkeit, um die vorgegebene Trajektorie abzufahren. Hierfür erhält er von der Trajektoriengenerierung die Trajektorie und von dem Ego Motion Knoten die Ist-Position des Fahrzeuges. 
\end{description}

\section{Limitierungen des Modellfahrzeuges}

Durch die verwendete Hardware auf dem GTCC ergeben sich diverse Limitierungen. So kann der Lenkservomotor lediglich einen Lenkwinkel von $25^\circ$ stellen, obwohl Achsenseitig ein Lenkeinschlag von $35^\circ$ möglich wäre. \newline
Auch fehlt der Lenkvorrichtung eine Rückführung des Lenkwinkels,  sodass für die Lenkung lediglich eine Steuerung, aber keine Regelung möglich ist. \newline
Außerdem kann das Fahrzeug aufgrund der ungleichen Spur- und Sturzeinstellungen der Räder an der Hinterachse und einem leichten Spiel in der Lenkung nicht perfekt geradeaus fahren.

\section{Motivation für einen Bahnfolgeregler am Modellfahrzeug}

Auf dem Fahrzeug existiert und läuft bereits ein Trajektorienfolgeregler. Dieser entstand, genau wie eine Bahnplanung und Trajektoriengenerierung, in einer Abschlussarbeit. Demzufolge sieht die ROS-Struktur momentan wie folgt aus:
\begin{figure}[H]
\begin{center}
               \includegraphics[height = 70pt]{ROS1.pdf}
               \caption{ROS-Knoten mit Trajektorienfolgereger}
               \label{img:Einspurmodell}
\end{center}
\end{figure}
Da in diesem Projekt Bahnen geplant und diese anschließend zu Trajektorien konvertiert werden, macht es grundsätzlich mehr Sinn direkt diese Bahn zu regeln. Um also den Schritt der Trajektoriengenerierung zu überspringen, soll ein Bahnfolgeregler entwickelt werden, wodurch die ROS-Struktur wie folgt aussehen soll:\begin{figure}[H]
\begin{center}
               \includegraphics[height = 50pt]{ROS2.pdf}
               \caption{ROS-Knoten mit Bahnfolgereger}
               \label{img:Einspurmodell}
\end{center}
\end{figure}
