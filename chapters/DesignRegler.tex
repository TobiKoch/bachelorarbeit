\chapter{Design des Bahnfolgereglers} \label{chap:Design}

\section{Auswahl des Algorithmus} 
Für die Umsetzung des Bahnfolgereglers in diesem Projekt wurden die Ansätze aus Kapitel \ref{sec:geo} und Kapitel \ref{sec:mod} anhand verschiedener Kriterien bewertet. Diese waren Umsetzbarkeit sowie Performanz bei Transferfahrt und Einparkvorgängen. \newline
Aufgrund mehrerer defekter Sensoren auf dem Fahrzeug in der Phase der Algorithmenwahl konnten keine Messfahrten getätigt werden. Dadurch war eine Evaluation, ob das GTCC modellierbar ist, nicht möglich. Die allgemeine Modellierbarkeit des Modellfahrzeuges ist fraglich, da es aufgrund nicht exakt eingestellter Räder und einem Spiel in der Lenkung nicht zuverlässig geradeaus fährt. Deswegen war die Umsetzung eines modellbasierten Ansatzes nicht möglich. \newline
Bei den geometrischen Regelansätzen wurden der Vector Pursuit und Pure Pursuit Algorithmus genauer diskutiert. In den hier auftretenden Szenarien weisen beide Algorithmen vergleichbare Ergebnisse auf. Dies liegt daran, dass in diesem Projekt die Lokalisierung keine Sprünge enthält, wie es beispielsweise bei GPS-Ortung der Fall wäre. Die geplanten Bahnen sind statisch und enthalten ebenfalls keine Sprünge. Dadurch ist der Vorteil des Vector Pursuit Algorithmus, mit Sprüngen umgehen zu können, in diesem Projekt nicht relevant. Deswegen wurde für diese Arbeit der Pure Pursuit Algorithmus ausgewählt. \newline
Für die Umsetzung des Bahnfolgereglers wurde ein ROS-Knoten in Python geschrieben. Als Eingabe erhält der Bahnfolgereglerknoten eine n x 4 Matrix, wobei n für eine endliche Abfolge an Punkten steht. In den 4 Spalten stehen jeweils die x- und y-Koordinate des anzufahrenden Punktes, die dort gewünschte Orientierung $\theta_{\mathrm{soll}}$ und die Fahrtrichtung, mit der dieser Punkt erreicht werden soll. \newline
Aufgrund der Lenkwinkellimitierung von $25^\circ$ wurde die Bahnplanung auf einen maximalen Lenkwinkel von $22^\circ$ limitiert, sodass der Regler über eine Stellgrößenreserve verfügt.
 
\section{Erweiterung und Optimierung des Algorithmus} \label{sec:UmsetzAlg}
Der Pure Pursuit Algorithmus hat in seiner Grundform parameterabhängige Schwächen. Die Implementierung dieser Grundform ist in Kapitel \ref{sec:pp} im Abschnitt \textit{Pure Pursuit Algorithmus} dargestellt. Die Schwächen dieses Algorithmus können durch eine gute Parametrierung und die niedrigen gefahrenen Geschwindigkeiten des GTCC fast vollständig unterdrückt werden. Diese optimierte Parametrierung und Erweiterung des Algorithmus wird im Folgenden erklärt.
\subsection{Optimierung der Geschwindigkeit}
Für das Modellfahrzeug wurde der Geschwindigkeitsbereich von -0,2\,$\frac{\mathrm{m}}{\mathrm{s}}$ bis 0,5\,$\frac{\mathrm{m}}{\mathrm{s}}$ festgelegt. Dabei soll der Geschwindigkeitsbetrag nicht kleiner 0,1\,$\frac{\mathrm{m}}{\mathrm{s}}$ sein. Damit das Fahrzeug besser durch Kurven fahren kann, wird die Geschwindigkeit dynamisch in Abhängigkeit der Krümmung der gefahrenen Bahn berechnet.
Hierfür wird die maximale fahrbare Krümmung durch den maximalen Lenkwinkel $\delta_{max}$ von $25^{\circ}$ und den Radstand $L_{\mathrm{R}}$ von $0{,}36\,\mathrm{m}$ nach (\ref{eq:kappa}) berechnet und daraus eine Rechenvorschrift für die krümmungsabhängige Geschwindigkeit abgeleitet:
\begin{equation} \label{eq:kappa}
\kappa_{\mathrm{max}} = \frac{\tan\delta_{\mathrm{max}}}{L_{\mathrm{R}}}
\end{equation}
\begin{equation} \label{eq:speed}
v = 
\begin{cases}
v_{\mathrm{min}} + (v_{\mathrm{max}}-v_{\mathrm{min}}) \cdot (1 - \frac{|\kappa|}{\kappa_{\mathrm{max}}}) & \text{für } |\kappa| \le \kappa_{\mathrm{max}} \\
v_{\mathrm{min}} & \text{sonst}
\end{cases}
\end{equation}
Die Berechnungsvorschrift für die Geschwindigkeit lautet dadurch für Vorwärtsfahrten beim GTCC:
\begin{equation}
v = 
\begin{cases}
0{,}1\frac{\mathrm{m}}{\mathrm{s}} + 0{,}4 \frac{\mathrm{m}}{\mathrm{s}} \cdot (1 - 0{,}7720\,\mathrm{m} \cdot |\kappa|) & \text{für } |\kappa| \le 1,295\mathrm{\frac{1}{m}} \\
0{,}1 \frac{\mathrm{m}}{\mathrm{s}} & \text{sonst}
\end{cases}
\end{equation}
und für Rückwärtsfahrten:
\begin{equation}
v =
\begin{cases}
-0{,}1\frac{\mathrm{m}}{\mathrm{s}} - 0{,}1 \frac{\mathrm{m}}{\mathrm{s}} \cdot (1 - 0{,}7720\,\mathrm{m} \cdot |\kappa|) & \text{für } |\kappa| \le 1,295\mathrm{\frac{1}{m}} \\
-0{,}1 \frac{\mathrm{m}}{\mathrm{s}} & \text{sonst}
\end{cases}
\end{equation}

\subsection{Optimierung der Look-Ahead Distanz}
Abhängig von der Look-Ahead Distanz zeigen sich bei dem Pure Pursuit Algorithmus zwei verschiedene Schwächen. Ist die Look-Ahead Distanz zu gering gewählt, so tendiert das Fahrzeug um den Pfad herum zu oszillieren. Wählt man diese aber zu groß, so werden die Kurven geschnitten. Daraus folgt, dass man auf einer geraden Strecke eine große Look-Ahead Distanz benötigt, um nicht um die Bahn zu oszillieren, während in Kurven eine kleine Look-Ahead Distanz für besseres Bahnfolgen sorgt.
Deshalb wird in dieser Umsetzung des Pure Pursuit Algorithmus die Look-Ahead Distanz dynamisch abhängig von der Geschwindigkeit $v$ nach folgender Vorschrift berechnet:
\begin{equation}
LAD = |v(\kappa)| \cdot 1{,}5\,\mathrm{s} 
\end{equation}
Da der Betrag der Geschwindigkeit von 0,1\,$\frac{\mathrm{m}}{\mathrm{s}}$ bis 0,5\,$\frac{\mathrm{m}}{\mathrm{s}}$ reicht, ergibt sich für die Look-Ahead Distanz ein Bereich von 0,15\,m bis 0,75\,m. Dies wurde in einer Geraudeausfahrt und einer Sinusfahrt mit verschiedenen Look-Ahead Distanzen als der optimale Bereich bestimmt, in dem das Fahrzeug an der Untergrenze gut den Kurven folgt und an der oberen Grenze nicht um den Pfad oszilliert.

\subsection{Erweiterung um die Beachtung der Orientierung}
Der Pure Pursuit Algorithmus beachtet in seiner Grundform nicht die Orientierung am Zielpunkt. Es ist zwar implizit gegeben, dass bei einer optimalen Bahnfolge auch die richtige Orientierung in jedem Punkt erreicht wird, aber wenn der Algorithmus von der Bahn abkommt, können große Orientierungsfehler auftreten. Dies ist vor allem bei einem Einparkmanöver zu vermeiden, da das Fahrzeug ansonsten mit den umliegenden Hindernissen kollidieren könnte. \newline
In dieser Arbeit wird die Beachtung der Orientierung nach dem Ansatz von \cite{PPPOrientierung} umgesetzt. Das Konzept hinter diesem Ansatz liegt darin, einen Fehler in x- und y- Richtung zu akzeptieren, wenn dadurch die Orientierung am Zielpunkt besser erreicht wird.
Hierfür wird der Zielpunkt, den der Pure Pursuit Algorithmus in diesem Zeitschritt anfahren möchte, um eine Distanz $d$ verschoben.
Die benötigte Distanz $d$, um den Zielpunkt in der richtigen Orientierung zu erreichen berechnet sich aus der Fahrzeugorientierung $\theta$, der Orientierung am Zielpunkt $\theta_{\mathrm{soll}}$, dem Look-Ahead Winkel $\eta_{\mathrm{d}}$ sowie der Ist-, als auch der Soll-Position wie folgt:
\begin{equation}
\eta_{\mathrm{d}}= \frac{\theta_{\mathrm{soll}} - \theta}{2}
\end{equation}
\begin{equation}
d = \frac{-\sin(\theta + \eta_{\mathrm{d}}) \cdot (x_{\mathrm{ist}} - x_{\mathrm{soll}}) + \cos(\theta + \eta_{\mathrm{d}}) \cdot (y_{\mathrm{ist}} - y_{\mathrm{soll}})}{\cos(\theta + \eta_{\mathrm{d}}) \cdot \sin(\theta_{\mathrm{soll}} + \frac{\pi}{2}) - \cos(\theta_{\mathrm{soll}} + \frac{\pi}{2}) \cdot \sin(\theta + \eta_{\mathrm{d}})}
\end{equation}
Die Distanz $d$, um die der Zielpunkt verschoben wird, hat eine Obergrenze, welche Szenario entsprechend gewählt werden sollte. In dieser Arbeit wurde die maximal tolerierte Abweichung auf 2,5\,cm festgelegt.\newline
Der transformierte Zielpunkt berechnet sich dadurch zu:
\begin{equation}
\begin{bmatrix}
x_{\mathrm{soll,\,transformiert}} \\
y_{\mathrm{soll,\,transformiert}}
\end{bmatrix}
= 
\begin{bmatrix}
x_{\mathrm{soll}} \\
y_{\mathrm{soll}}
\end{bmatrix}
-
\begin{bmatrix}
d \cdot \cos(\theta_{\mathrm{soll}} + \frac{\pi}{2}) \\
d \cdot \sin(\theta_{\mathrm{soll}} + \frac{\pi}{2})
\end{bmatrix}
\end{equation}
Der transformierte Zielpunkt wird dem Pure Pursuit Algorithmus übergeben. \newline
In Abbildung \ref{img:Erweiterung} ist diese Erweiterung dargestellt. Der blaue Punkt und das blaue Fahrzeug stellen die Soll-Position dar. Würde man nach der Geometrie des normalen Pure Pursuit Algorithmus zu diesem Punkt fahren, würde man dem grünen Bogen folgen und käme mit der falschen Orientierung an. Deswegen verschiebt man den Soll-Punkt um eine Distanz $d$ und erhält dadurch die Soll-$\mathrm{Position_{transformiert}}$, die durch den roten Punkt dargestellt wird. Berechnet man nun mit dem normalen Pure Pursuit Algorithmus den Kreisbogen zu diesem Punkt, erhält man den roten Bogen und kommt zwar mit einem Positionsfehler am Soll-Punkt an, erreicht diesen aber in der richtigen Orientierung. 

\begin{figure}[h]
\begin{center}
               \includegraphics[height = 200pt]{Erweiterung.pdf}
               \caption{Darstellung der Verschiebung des Soll-Punktes für das Erreichen mit einer besseren Orientierung}
               \label{img:Erweiterung}
\end{center}
\end{figure}

Man kann in Abbildung \ref{img:Erweiterung} gut erkennen, dass es bei einem Einparkvorgang Sinn macht, einen Positionsfehler zu akzeptieren, um mit einer besseren Orientierung die Zielposition zu erreichen. Man steht um eine Abweichung $d$ neben dem eigentlichen Zielpunkt, aber ragt nicht schräg aus der Parklücke heraus.  
