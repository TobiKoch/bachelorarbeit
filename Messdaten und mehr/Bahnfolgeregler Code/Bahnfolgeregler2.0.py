#!/usr/bin/env python
import csv
import rospy
import numpy as np
import math
import matplotlib.pyplot as plt
from gtcc_msgs.msg import Ros_Bridge
from gtcc_msgs.msg import gtcc_odometry
from gtcc_msgs.msg import geometry_msgs
from ackermann_msgs.msg import AckermannDriveStamped
from time import sleep
from prettytable import PrettyTable


# Initialisierung der Werte
LAD = 0.2 #Look-Ahead-Distance [m], falls statisch gewünscht
LADd = 1.5 #für dynamische Berechnung. LADd*abs(drivespeed) = LAD, 0 setzen für statische LAD, 1 ist Standard
drivespeed_min = -0.2 #drivespeed max rückwärts[m/s]
drivespeed_max = 0.5 #drivespeed max vorwärts[m/s]
wheelbase=0.36 #Radstand des Fahrzeuges
tol = 0.015  # Toleranz zur Zielposition [m], wenn Wert zu klein ist, stoppt das Fahrzeug eventuell erst hinter dem Punkt
bahneinlesen = 1 # 1 fuer Bahn die richtig herum gespeichert ist, 2 fuer Bahn zB aus Nehas Generierung, die falsch herum ist, 3 fuer Bahn aus Funktionen
plot = 1 # 1, damit am Ende ein Plot erstellt wird, 0 für keinen Plot

#Diese Werte nicht verändern, werden initialisiert, damit alles richtig funktioniert, aber haben keinen Einfluss auf den Code
goaldist = tol+1
xy_ist = [0, 0, 0] #starting point in x,y,theta



#Bahn aus bahn.csv oder Funktion einlesen.
if bahneinlesen==1:
	with open('/home/gtcc1/Dropbox/bahn.csv', 'r') as f:
		reader = csv.reader(f, delimiter=';')
		content = np.array(list(reader))
		print('Bahn eingelesen. Bahnfolge startet.')
		content = content.astype(float)

	x_soll = np.array(content[:, 0])
	y_soll = np.array(content[:, 1])
	theta_soll = np.array(content[:,2])
	v_soll = np.array(content[:, 3])
elif bahneinlesen==2:
	with open('/home/gtcc1/Dropbox/bahn.csv', 'r') as f:
		reader = csv.reader(f, delimiter=';')
		content = np.array(list(reader))
		content = content.astype(float)
	#print(np.flipud(content))
	x_soll = np.flipud(content[:, 0])
	y_soll = np.flipud(content[:, 1])
	v_soll = np.flipud(content[:, 3])
	#print(x_soll, y_soll, v_soll)
elif bahneinlesen==3:
	x_soll=np.array([0, 2,  4, 7])
	#y_soll=np.array([0, 1, 2, 3, 4, 6, 6, 5, 3, 1])
	#v_soll=np.array([1, 1, 1, 1, -1, -1, -1, -1, -1, -1])
	#x_soll = np.linspace(0.0,2, num=20)
	#x_soll = 2*np.sin(x) #x component of path
	#y_soll = 0.01*np.sin(3*x_soll) #y component of path
	theta_soll=np.zeros(4)
	y_soll = np.zeros(4)
	v_soll = np.ones(4)



# Definitionen der Funktionen
def lenkwinkel_berechnen(x_soll, y_soll,v_soll, xy_ist, index_soll):
	if index_soll < v_soll.size:
		dx = x_soll-xy_ist[0]
		dy = y_soll-xy_ist[1]
		alpha = math.atan2(dy,dx)-xy_ist[2] #Orientierungsdifferenz von Fzg zu Zielpunkt
		if (v_soll[index_soll] < 0):
			alpha=np.pi-alpha  #Drehung der Orientierungsdifferenz, falls rückwärts gefahren wird
		K = 2*np.sin(alpha)/math.hypot(dx,dy)   #Kruemmung der gewuenschten Bahn berechnen
		delta = np.arctan(K*wheelbase)  #Lenkwinkelberechnung
	return delta, K, alpha

def index_soll_berechnen(xy_ist, x_soll, y_soll, LAD, index_soll):
	dist = math.hypot(x_soll[index_soll]-xy_ist[0], #Abstand zwischen Soll und Ist-Punkt
	                y_soll[index_soll]-xy_ist[1])
	if dist < 0.05 and index_soll+1 < x_soll.size:
		index_soll = index_soll +1
	while dist < LAD and index_soll+1 < x_soll.size: #sucht in einem Radius nach dem nächsten anzufahrenden Punkt
		dx = x_soll[index_soll+1]-x_soll[index_soll]
		dy = y_soll[index_soll+1]-y_soll[index_soll]
		dist = dist + math.hypot(dx, dy) #Update den Abstand zum nächsten Soll-Punkt
		if v_soll[index_soll]==v_soll[index_soll+1] and dist < LAD: #falls Fahrtrichtungswechsel stattfindet wird erstmal bis zum Punkt gefahren, wo gewechselt wird
			index_soll = index_soll+1
		elif dist < LAD:
			if dist > 0.02:
				return index_soll
			else:
				msg.drive.speed=0.0
				pub.publish(msg)
				sleep(0.5)
				return index_soll+1
	return index_soll

def pos_soll_transform(x_soll, y_soll, theta_soll, xy_ist, index_soll):
	etad = (theta_soll[index_soll] - xy_ist[2])/2
	a = theta_soll[index_soll] + np.pi/2
	b = xy_ist[2] + etad
	d = ((-np.sin(b)*(xy_ist[0]-x_soll[index_soll])) + np.cos(b)*(xy_ist[1]-y_soll[index_soll]))/(np.cos(b)*np.sin(a)- np.cos(a)*np.sin(b))
	if (abs(d) > 0.025):
		d = 0.025 * np.sign(d)
	x_soll_t = x_soll[index_soll] - d*np.cos(a)
	y_soll_t = y_soll[index_soll] - d*np.sin(b)
	return x_soll_t, y_soll_t


def callback(data):
	xy_ist[0] = data.position.x
	xy_ist[1] = data.position.y
	xy_ist[2] = data.orientation.z

def listener():
	pass

def talker():
	pass

def emergencybrake():
	msg.drive.speed=0.0
	pub.publish(msg)


	

if __name__ == '__main__':
	
	#ROS Initialisierungen
	rospy.init_node('Bahnfolgeregler', anonymous=True)
	pub = rospy.Publisher('TC_msg', AckermannDriveStamped, queue_size=1000)
	rospy.Subscriber("EM_msg", gtcc_odometry, callback)
	rate = rospy.Rate(100)  # 100Hz
	msg = AckermannDriveStamped()
	sleep(0.1) #Wartezeit, dass Subscriber läuft, damit richtiger index_soll ausgerechnet werden kann

	#Einstiegs Index_soll berechnen, indem man den Punkt der Bahn findet, dem das Fahrzeug am naechsten ist
	dx=x_soll - xy_ist[0]
	dy=y_soll - xy_ist[1]
	abstaende=np.sqrt(np.square(dx)+np.square(dy))
	index_soll=np.argmin(abstaende) + 1
	
	#Initialisierungen für den Plot
	xdata=np.array([xy_ist[0]])
	ydata=np.array([xy_ist[1]])
	
	#Pure Pursuit Algorithmus
	while (not rospy.is_shutdown()) and (((goaldist > tol) or (index_soll < x_soll.size))):
		
		
		#Berechnet den Index von dem naechsten Zielpunkt
		index_soll=index_soll_berechnen(xy_ist, x_soll, y_soll, LAD, index_soll) 
		#Berechnet benötigten Lenkwinkel, um zum nächsten Punkt zu kommen. Gibt außerdem Krümmung zurück
		x_soll_t, y_soll_t = pos_soll_transform(x_soll, y_soll, theta_soll, xy_ist, index_soll)
		delta, K, alpha=lenkwinkel_berechnen(x_soll_t,y_soll_t,v_soll,xy_ist,index_soll)
		if(goaldist<0.04):
			delta = 0  	
		msg.drive.steering_angle = delta	
		#Berechnet den Abstand zum finalen Zielpunkt
		goaldist=math.hypot(xy_ist[0]-x_soll[x_soll.size-1], xy_ist[1]-y_soll[y_soll.size-1]) 
		#Adaptive Geschwindigkeitssteuerung abhaengig von der Kruemmung der gefahrenen Kurve
		if v_soll[index_soll] == 1: #Entscheidung, ob vorwaerts oder rueckwaerts gefahren wird

			drivespeed = 0.1+0.4*(1-0.772*abs(K)) if abs(K)<1.295 else 0.1
			if drivespeed > drivespeed_max:
				drivespeed = drivespeed_max
		else:
			drivespeed = -0.1-0.1*(1-0.772*abs(K)) if abs(K)<1.295 else -0.1
			if drivespeed < drivespeed_min:
				drivespeed = drivespeed_min	
		if ((goaldist > tol or index_soll < x_soll.size-1)and ((abs(alpha)<(np.pi/2)) or (abs(alpha)>(3*np.pi/2)))):
			msg.drive.speed=drivespeed
		else:
			msg.drive.steering_angle=-100.0 #hard gecoded, durch delta=-100 wird alles auf 0 gesetzt.
			msg.drive.speed=0.0
			pub.publish(msg)
			print('Parkvorgang abgeschlossen.') if ((abs(alpha)<(np.pi/2)) or (abs(alpha)>(3*np.pi/2))) else print('Abbruch des Parkvorgangs.') 
			break

		#Dynamsiche Berechnung der LAD abhängig von der Geschwindigkeit	
		if (LADd != 0):
			LAD = abs(drivespeed) * LADd 
		#Published die TC_msg und setzt dadurch Geschwindigkeit und Lenkwinkel
		pub.publish(msg)
		
		#Speichern der Ist-Position für späteren Plot
		xdata=np.append(xdata,[xy_ist[0]])
		ydata=np.append(ydata,[xy_ist[1]])
		#Ausgabefenster
		p = PrettyTable()
		p.field_names = ["Index Soll", "Lenkwinkel [°]", "Zieldistanz [m]", "Geschwindigkeit [m/s]"]
		p.add_row([index_soll, round(delta*180/np.pi,3), round(goaldist,2), round(drivespeed,2)])
		print("\033c")
		print(p.get_string(title="Bahnfolgeregler Ausgaben"))
		
		
		
		
		rospy.on_shutdown(emergencybrake)
		rate.sleep()
	#Ende des Pure Pursuit Algorithmus und der While Schleife
		
if plot == 1:
	# Vorbereitung Fahrzeug Plot
	x1=(-0.13)*np.cos(xy_ist[2])-(-0.16)*np.sin(xy_ist[2])+xy_ist[0]
	x2=(-0.13)*np.cos(xy_ist[2])-(0.16)*np.sin(xy_ist[2])+xy_ist[0]
	x3=0.53*np.cos(xy_ist[2])-(0.16)*np.sin(xy_ist[2])+xy_ist[0]
	x4=0.53*np.cos(xy_ist[2])-(-0.16)*np.sin(xy_ist[2])+xy_ist[0]
	y1=(-0.13)*np.sin(xy_ist[2])+(-0.16)*np.cos(xy_ist[2])+xy_ist[1]
	y2=(-0.13)*np.sin(xy_ist[2])+(0.16)*np.cos(xy_ist[2])+xy_ist[1]
	y3=0.53*np.sin(xy_ist[2])+(0.16)*np.cos(xy_ist[2])+xy_ist[1]
	y4=0.53*np.sin(xy_ist[2])+(-0.16)*np.cos(xy_ist[2])+xy_ist[1]
	x = [x1, x2, x3, x4, x1]
	y = [y1, y2, y3, y4, y1]

	#Vorbereitung Parkplatz Plot
	x1=(-0.175)*np.cos(theta_soll[-1])-(-0.21)*np.sin(theta_soll[-1])+x_soll[-1]
	x2=(-0.175)*np.cos(theta_soll[-1])-(0.21)*np.sin(theta_soll[-1])+x_soll[-1]
	x3=0.575*np.cos(theta_soll[-1])-(0.21)*np.sin(theta_soll[-1])+x_soll[-1]
	x4=0.575*np.cos(theta_soll[-1])-(-0.21)*np.sin(theta_soll[-1])+x_soll[-1]
	y1=(-0.175)*np.sin(theta_soll[-1])+(-0.21)*np.cos(theta_soll[-1])+y_soll[-1]
	y2=(-0.175)*np.sin(theta_soll[-1])+(0.21)*np.cos(theta_soll[-1])+y_soll[-1]
	y3=0.575*np.sin(theta_soll[-1])+(0.21)*np.cos(theta_soll[-1])+y_soll[-1]
	y4=0.575*np.sin(theta_soll[-1])+(-0.21)*np.cos(theta_soll[-1])+y_soll[-1]
	x1 = [x1, x2, x3, x4, x1]
	y1 = [y1, y2, y3, y4, y1]


	plt.axis([min(x_soll)-1, max(x_soll)+1, min(y_soll)-1, max(y_soll)+1])	
	plt.plot(x_soll,y_soll, ".b", label="Soll-Bahn")
	plt.plot(xdata[::20],ydata[::20], ".r", label="Ist-Bahn")
	plt.plot(x, y, "r-", label="Fahrzeugposition")
	plt.plot(x1,y1,"b-", label="Parkplatz")
	plt.legend()
	plt.title("Vergleich zwischen Soll-Bahn und Ist-Bahn")
	plt.xlabel("x [m]")
	plt.ylabel("y [m]")
	plt.axis("equal")
	plt.show()
	
