%Berechnet den Fehler zwischen der Ist und Soll Bahn
clear
clearvars total_error x_error y_error orient_error

name_ist=dir('*.txt');
istdata = csvread(name_ist(1).name,1,5);
idx = find(istdata(:,1)~=0, 1, 'first'); %Index des ersten Eintrages ungleich 0
%dx = find(round(istdata(:,1),3)~=round(istdata(end,1),3),1, 'last')+1; %Index des letzten relevanten Punktes
dx = length(istdata);
xy_ist(:,1)=istdata(idx:dx,1);
xy_ist(:,2)=istdata(idx:dx,2);
xy_ist(:,3)=istdata(idx:dx,6);

name_soll=dir('*.csv');
xy_soll=dlmread(name_soll.name, ';');


k=1;
for i=1:length(xy_soll)
        %[M,D]=min(abs(xy_ist(:,1)-xy_soll(i,1)));
        %[K,E]=min(abs(xy_ist(:,2)-xy_soll(i,2)));
        %x-Fehler index berechnen
        dist=sqrt((xy_ist(:,1)-xy_soll(i,1)).^2+(xy_ist(:,2)-xy_soll(i,2)).^2);
        [M, k]=min(dist);
        y_error(i)=xy_ist(k,2)-xy_soll(i,2);
        x_error(i)=xy_ist(k,1)-xy_soll(i,1);
        orient_error(i)=xy_ist(k,3)-xy_soll(i,3);
        total_error(i)=min(dist);
        i=i+1;
        k=k+1;
end

%Plot Vorbereitung
xy_ist_plot=xy_ist(1:floor(length(xy_ist)/40):end,:);
xy_ist_plot(42,:)=xy_ist(end,:);
%Plot Sachen
 figure
  
 subplot(2,3,[1, 2 ,3])
 labels = cellstr( num2str([1:length(xy_soll)]'));
 plot(xy_ist_plot(:,1), xy_ist_plot(:,2), 'r', 'LineWidth', 1.5)
 hold on
 %plot(xy_soll(1,1),xy_soll(1,2))
 plot(xy_soll(1:floor(length(xy_soll)/30):end,1),xy_soll(1:floor(length(xy_soll)/30):end,2),'bx', 'LineWidth', 1.0)
 text(xy_soll(1:floor(length(xy_soll)/10):end,1),xy_soll(1:floor(length(xy_soll)/10):end,2),labels(1:floor(length(xy_soll)/10):end,1), 'VerticalAlignment','bottom', 'HorizontalAlignment','right')
 %Parkplatz Plot L�nge 75cm Breite 42cm
 x1=(-0.175)*cos(xy_soll(length(xy_soll),3))-(-0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x2=(-0.175)*cos(xy_soll(length(xy_soll),3))-(0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x3=0.575*cos(xy_soll(length(xy_soll),3))-(0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x4=0.575*cos(xy_soll(length(xy_soll),3))-(-0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 y1=(-0.175)*sin(xy_soll(length(xy_soll),3))+(-0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y2=(-0.175)*sin(xy_soll(length(xy_soll),3))+(0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y3=0.575*sin(xy_soll(length(xy_soll),3))+(0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y4=0.575*sin(xy_soll(length(xy_soll),3))+(-0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 x = [x1, x2, x3, x4, x1];
 y = [y1, y2, y3, y4, y1];
 plot(x, y, 'b-');
 %Fahrzeug Plot
 x1=(-0.13)*cos(xy_ist(length(xy_ist),3))-(-0.16)*sin(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),1);
 x2=(-0.13)*cos(xy_ist(length(xy_ist),3))-(0.16)*sin(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),1);
 x3=0.53*cos(xy_ist(length(xy_ist),3))-(0.16)*sin(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),1);
 x4=0.53*cos(xy_ist(length(xy_ist),3))-(-0.16)*sin(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),1);
 y1=(-0.13)*sin(xy_ist(length(xy_ist),3))+(-0.16)*cos(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),2);
 y2=(-0.13)*sin(xy_ist(length(xy_ist),3))+(0.16)*cos(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),2);
 y3=0.53*sin(xy_ist(length(xy_ist),3))+(0.16)*cos(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),2);
 y4=0.53*sin(xy_ist(length(xy_ist),3))+(-0.16)*cos(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),2);
 x = [x1, x2, x3, x4, x1];
 y = [y1, y2, y3, y4, y1];
 plot(x, y, 'k-');
 legend('Bahn_{ist}', 'Bahn_{soll}', 'Parkplatz', 'Fahrzeug')
 subplot(2,3,1:3)
 xlabel('x [m]')
 ylabel('y [m]')
 axis equal
 title('Soll- und Ist-Bahn im Vergleich')
 
 subplot(2,3,5)
 plot(y_error*100, 'color',[217 2 28]./255, 'LineWidth', 1.5)
 title('Y-Fehler')
 xlabel('Position_{soll}')
 ylabel('Fehler [cm]')
 legend('Y-Fehler')
 
 subplot(2,3,4)
 plot(x_error*100, 'color', [217 83 25]./255, 'LineWidth', 1.5)
 title('X-Fehler')
 xlabel('Position_{soll}')
 ylabel('Fehler [cm]')
 legend('X-Fehler')
 
 subplot(2,3,6)
 plot(orient_error*180/pi, 'color', [217 211 12]./255, 'LineWidth', 1.5)
 title('Orientierungsfehler')
 xlabel('Position_{soll}')
 ylabel('Fehler [�]')
 legend('Orientierungsfehler')
 
 
 
 %2. Figure f�r genaueres Betrachten der finalen Position.
 figure
 subplot(1,2,1)
 x1=(-0.175)*cos(xy_soll(length(xy_soll),3))-(-0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x2=(-0.175)*cos(xy_soll(length(xy_soll),3))-(0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x3=0.575*cos(xy_soll(length(xy_soll),3))-(0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x4=0.575*cos(xy_soll(length(xy_soll),3))-(-0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 y1=(-0.175)*sin(xy_soll(length(xy_soll),3))+(-0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y2=(-0.175)*sin(xy_soll(length(xy_soll),3))+(0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y3=0.575*sin(xy_soll(length(xy_soll),3))+(0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y4=0.575*sin(xy_soll(length(xy_soll),3))+(-0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 x = [x1, x2, x3, x4, x1];
 y = [y1, y2, y3, y4, y1];
 plot(x, y, 'b-');
 axis equal
 hold on
 %Fahrzeug Plot
 x1=(-0.13)*cos(xy_ist(length(xy_ist),3))-(-0.16)*sin(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),1);
 x2=(-0.13)*cos(xy_ist(length(xy_ist),3))-(0.16)*sin(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),1);
 x3=0.53*cos(xy_ist(length(xy_ist),3))-(0.16)*sin(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),1);
 x4=0.53*cos(xy_ist(length(xy_ist),3))-(-0.16)*sin(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),1);
 y1=(-0.13)*sin(xy_ist(length(xy_ist),3))+(-0.16)*cos(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),2);
 y2=(-0.13)*sin(xy_ist(length(xy_ist),3))+(0.16)*cos(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),2);
 y3=0.53*sin(xy_ist(length(xy_ist),3))+(0.16)*cos(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),2);
 y4=0.53*sin(xy_ist(length(xy_ist),3))+(-0.16)*cos(xy_ist(length(xy_ist),3))+xy_ist(length(xy_ist),2);
 x = [x1, x2, x3, x4, x1];
 y = [y1, y2, y3, y4, y1];
 plot(x, y, 'k-');
 title('Finale Fahrzeugposition')
 xlabel('x [m]')
 ylabel('y [m]')
 legend('Parkplatz', 'Fahrzeug')
 
 
 
 subplot(1,2,2)
fehler=[x_error(end); y_error(end); orient_error(end)];
% errors=[x_error(end)*100; y_error(end)*100; orient_error(end)*180/pi];
% a = bar(errors,'k');
% a(:,1).FaceColor = [217 83 25]./255;
% a(:,2).FaceColor = [217 2 28]./255;
% a(:,3).FaceColor = [217 211 12]./255;

a = bar(1,x_error(end)*100, 0.3);
a.FaceColor=[217 83 25]./255;
hold on
b = bar(2,y_error(end)*100, 0.3);
b.FaceColor=[217 2 28]./255;
c = bar(3,orient_error(end)*180/pi,0.3);
c.FaceColor=[217 211 12]./255;
title('Abweichungen von der Zielposition')
ylabel('X-Fehler [cm], Y-Fehler [cm], Orientierungsfehler [�]')
legend('X-Fehler', 'Y-Fehler', 'Orientierungsfehler')
set(gca,'xtick',[])

%print -depsc Fehlerauswertung.eps