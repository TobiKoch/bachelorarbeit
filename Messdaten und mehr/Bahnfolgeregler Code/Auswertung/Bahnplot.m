figure
set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex')
set(groot, 'defaultLegendInterpreter','latex')
subplot(2,2,[1,2])
plot(bahn(:,1),bahn(:,2),'-x', 'color', 'b', 'LineWidth', 1.25')
hold on
axis equal
xy_soll=bahn;
x1=(-0.175)*cos(xy_soll(length(xy_soll),3))-(-0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x2=(-0.175)*cos(xy_soll(length(xy_soll),3))-(0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x3=0.575*cos(xy_soll(length(xy_soll),3))-(0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x4=0.575*cos(xy_soll(length(xy_soll),3))-(-0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 y1=(-0.175)*sin(xy_soll(length(xy_soll),3))+(-0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y2=(-0.175)*sin(xy_soll(length(xy_soll),3))+(0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y3=0.575*sin(xy_soll(length(xy_soll),3))+(0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y4=0.575*sin(xy_soll(length(xy_soll),3))+(-0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 x = [x1, x2, x3, x4, x1];
 y = [y1, y2, y3, y4, y1];
 plot(x, y, 'b-', 'LineWidth', 1.0);
 title('Einparkszenario ,,R\"uckw\"arts parallel"')
 xlabel('x [m]')
 ylabel('y [m]')
 legend('Soll-Bahn', 'Parkplatz')
 box off
 
 
 subplot(2,2,3)
plot(bahn1(1:2:end,1),bahn1(1:2:end,2),'-x', 'color', 'b', 'LineWidth', 1.25')
hold on
axis equal
xy_soll=bahn1;
x1=(-0.175)*cos(xy_soll(length(xy_soll),3))-(-0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x2=(-0.175)*cos(xy_soll(length(xy_soll),3))-(0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x3=0.575*cos(xy_soll(length(xy_soll),3))-(0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x4=0.575*cos(xy_soll(length(xy_soll),3))-(-0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 y1=(-0.175)*sin(xy_soll(length(xy_soll),3))+(-0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y2=(-0.175)*sin(xy_soll(length(xy_soll),3))+(0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y3=0.575*sin(xy_soll(length(xy_soll),3))+(0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y4=0.575*sin(xy_soll(length(xy_soll),3))+(-0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 x = [x1, x2, x3, x4, x1];
 y = [y1, y2, y3, y4, y1];
 plot(x, y, 'b-', 'LineWidth', 1.0);
 title('Einparkszenario ,,R\"uckw\"arts senkrecht"')
 xlabel('x [m]')
 ylabel('y [m]')
% legend('Soll-Bahn', 'Parkplatz')
 box off
 
 
 subplot(2,2,4)
plot(bahn2(1:2:end,1),bahn2(1:2:end,2),'-x', 'color', 'b', 'LineWidth', 1.25')
hold on
axis equal
xy_soll=bahn2;
x1=(-0.175)*cos(xy_soll(length(xy_soll),3))-(-0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x2=(-0.175)*cos(xy_soll(length(xy_soll),3))-(0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x3=0.575*cos(xy_soll(length(xy_soll),3))-(0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 x4=0.575*cos(xy_soll(length(xy_soll),3))-(-0.21)*sin(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),1);
 y1=(-0.175)*sin(xy_soll(length(xy_soll),3))+(-0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y2=(-0.175)*sin(xy_soll(length(xy_soll),3))+(0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y3=0.575*sin(xy_soll(length(xy_soll),3))+(0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 y4=0.575*sin(xy_soll(length(xy_soll),3))+(-0.21)*cos(xy_soll(length(xy_soll),3))+xy_soll(length(xy_soll),2);
 x = [x1, x2, x3, x4, x1];
 y = [y1, y2, y3, y4, y1];
 plot(x, y, 'b-',  'LineWidth', 1.0);
 title('Einparkszenario ,,Vorw\"arts senkrecht"')
 xlabel('x [m]')
 ylabel('y [m]')
 %legend('Soll-Bahn', 'Parkplatz')
 box off
 