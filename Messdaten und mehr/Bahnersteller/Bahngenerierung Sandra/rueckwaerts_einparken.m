clear all
close all
clc

%% Parameter
% Fahrzeugparameter
car.max_lenkwinkel = 15*pi/180; % maximaler Lenkwinkel [rad]
car.l_1 = 0.17; % Abstand Vorderachse - Sto�stange vorne [m]
car.l_2 = 0.13; % Abstand Hinterachse - Sto�stange hinten [m]
car.L = 0.36; % Radstand [m]pl
car.b = 0.27; % Spurweite [m]

% Parkhausparameter
parkhaus.h_cd = 2; % Breite der hindernisfreien Region vor dem Parkplatz [m]
parkhaus.sicherheitsabstand = 0.2; % Abstand zu parkenden Fahrzeugen [m]
parkhaus.pfad_aufl = 0.05; % Pfadaufl�sung [m]

% Parkplatzparameter
parkplatz.h_pd = 0.8; % Breite der Parkl�cke [m]
parkplatz.l_p = 1.5; % L�nge der Parkl�cke [m]
parkplatz.richtung = 0; % befindet sich der Parkplatz links oder rechts vom Fahrzeug? Bool! (0 = Parkplatz rechts des Fahrzeuges / 1 = Parkplatz links des Fahrzeuges)
parkplatz.x_pO = 0.5; 
parkplatz.y_pO = 3;
parkplatz.t_pO = pi/2;
x_p = -1*parkplatz.l_p + car.l_2 + 0.2;     % B: Endkoordinate Hinterachsmittelpunkt zum Parkl�ckenbeginn [m]


%% Algorithmus
rho_c = car.L/tan(car.max_lenkwinkel);   % B: Wendekreishalbmesser [m]
r_B2 = sqrt((car.L+car.l_1)...  % B: Radius f�r Punkt B2 [m]
    ^2+(rho_c+car.b/2)^2);
r_B4 = sqrt(car.l_2^2+...   % B: Radius f�r Punkt B4 [m]
    (rho_c+car.b/2)^2);

m_B_s_max = parkhaus.h_cd-r_B2;  % B: Minus Betrag von s (max)
m_B_s_min = -sqrt(...   % B: Minus Betrag von s (min)
    (rho_c-car.b/2)^2-...
    (rho_c-parkplatz.h_pd/2)^2);

x_O_S2_i = m_B_s_min;   % B: Mittelpunkt Kreissegment 2 (x min)
x_O_S2_a = m_B_s_max;   % B: Mittelpunkt Kreissegment 2 (x max)
x_O_S2_m = (x_O_S2_i... % B: Mittelpunkt Kreissegment 2 (x mittel)
    +x_O_S2_a)/2;
y_O_S2 = -rho_c;        % B: Mittelpunkt Kreissegment 2 (y)
x_O_S1 = car.b/2+rho_c...   % B: Mittelpunkt Kreissegment 1 (x)
    +parkhaus.sicherheitsabstand;
phi = acos((...         % B: Winkel phi f�r Berechnung y_O_S1
    parkhaus.sicherheitsabstand+rho_c...
    +car.b/2+abs...
    (x_O_S2_m))...
    /(2*rho_c));
y_O_S1 = rho_c*...      % B: Mittelpunkt Kreissegment 1 (y)
    (2*sin(phi)-1);

if x_O_S2_i < x_O_S2_a  % B: �berpr�fung, ob das Fahrzeug ohne Ber�hrung ausparken kann 
% Segment 1
ang_1 = (pi:(parkhaus.pfad_aufl/rho_c):pi+phi)';                 % Winkelvektor von pi bis pi+phi
xp1=rho_c*cos(ang_1);                       % x-Koordinaten des Kreises bezogen auf Ursprung
yp1=rho_c*sin(ang_1);                       % y-Koordinaten des Kreises bezogen auf Ursprung
x_1 = x_O_S1+xp1;                           % x-Koordinaten des Kreises bezogen auf x_O_S1
y_1 = y_O_S1+yp1;                           % y-Koordinaten des Kreises bezogen auf y_O_S1

% Segment 2
ang_2= (phi:(parkhaus.pfad_aufl/rho_c):pi/2)';                   % Winkelvektor von phi bis pi/2
xp2=rho_c*cos(ang_2);                       % x-Koordinaten des Kreises bezogen auf Ursprung
yp2=rho_c*sin(ang_2);                       % y-Koordinaten des Kreises bezogen auf Ursprung
x_2 = x_O_S2_m+xp2;                         % x-Koordinaten des Kreises bezogen auf x_O_S2
y_2 = y_O_S2+yp2;                           % y-Koordinaten des Kreises bezogen auf y_O_S2

% Segment 3
x_3 = (x_2(end,1):-parkhaus.pfad_aufl:x_p)';             % x-Vektor
y_3 = y_2(end,1)*ones(length(x_3),1);       % y-Vektor
ang_3 = (pi/2)*ones(length(x_3),1);   % Orientierungs-Vektor

%% Pfade definieren
pfad_31 = [x_1, y_1, mod((ang_1+pi/2),2*pi)];

for ii=1:1:size(pfad_31,1)
    pfad_31(ii,[1:2]) = ([[cos(parkplatz.t_pO), -sin(parkplatz.t_pO)];[sin(parkplatz.t_pO), cos(parkplatz.t_pO)]] * [pfad_31(ii,1); pfad_31(ii,2)])' + [parkplatz.x_pO, parkplatz.y_pO];
    pfad_31(ii,3) = mod(pfad_31(ii,3)+parkplatz.t_pO, 2*pi);
end

if parkplatz.richtung == 1
pfad_31(:,2) = -pfad_31(:,2);
pfad_31(:,3) = mod(-pfad_31(:,3), 2*pi);
else
pfad_31(:,2) = pfad_31(:,2);
pfad_31(:,3) = mod(pfad_31(:,3), 2*pi);   
end

pfad_32 = [x_2, y_2, mod((ang_2+1.5*pi),2*pi)];

for ii=1:1:size(pfad_32,1)
    pfad_32(ii,[1:2]) = ([[cos(parkplatz.t_pO), -sin(parkplatz.t_pO)];[sin(parkplatz.t_pO), cos(parkplatz.t_pO)]] * [pfad_32(ii,1); pfad_32(ii,2)])' + [parkplatz.x_pO, parkplatz.y_pO];
    pfad_32(ii,3) = mod(pfad_32(ii,3)+parkplatz.t_pO, 2*pi);
end

if parkplatz.richtung == 1
pfad_32(:,2) = -pfad_32(:,2);
pfad_32(:,3) = mod(-pfad_32(:,3), 2*pi);
else
pfad_32(:,2) = pfad_32(:,2);
pfad_32(:,3) = mod(pfad_32(:,3), 2*pi);   
end

pfad_33 = [x_3, y_3, mod((ang_3+1.5*pi),2*pi)];

for ii=1:1:size(pfad_33,1)
    pfad_33(ii,[1:2]) = ([[cos(parkplatz.t_pO), -sin(parkplatz.t_pO)];[sin(parkplatz.t_pO), cos(parkplatz.t_pO)]] * [pfad_33(ii,1); pfad_33(ii,2)])' + [parkplatz.x_pO, parkplatz.y_pO];
    pfad_33(ii,3) = mod(pfad_33(ii,3)+parkplatz.t_pO, 2*pi);
end

if parkplatz.richtung == 1
pfad_33(:,2) = -pfad_33(:,2);
pfad_33(:,3) = mod(-pfad_33(:,3), 2*pi);
else
pfad_33(:,2) = pfad_33(:,2);
pfad_33(:,3) = mod(pfad_33(:,3), 2*pi);   
end

else
    disp('collision into parking lot!')        % A: Anzeige n.OK
end

pfad = [pfad_31; pfad_32; pfad_33];

%% Plot
figure(1)
plot(pfad(:,1), pfad(:,2),'.-')
hold on
TM = [[cos(parkplatz.t_pO), -sin(parkplatz.t_pO)];[sin(parkplatz.t_pO), cos(parkplatz.t_pO)]];
br = TM*[-parkplatz.l_p; -parkplatz.h_pd/2] + [parkplatz.x_pO; parkplatz.y_pO];
fr = TM*[0; -parkplatz.h_pd/2] + [parkplatz.x_pO; parkplatz.y_pO];
fl = TM*[0; parkplatz.h_pd/2] + [parkplatz.x_pO; parkplatz.y_pO];
bl = TM*[-parkplatz.l_p; parkplatz.h_pd/2] + [parkplatz.x_pO; parkplatz.y_pO];
x_poly_parkplatz = [br(1,1), fr(1,1), fl(1,1), bl(1,1)];
y_poly_parkplatz = [br(2,1), fr(2,1), fl(2,1), bl(2,1)];
plot_parkplatz = plot(x_poly_parkplatz,y_poly_parkplatz);
fill_plot_parkplatz = fill(x_poly_parkplatz,y_poly_parkplatz,'r','facealpha',.5);
axis equal
clear TM br fr fl bl x_poly_parkplatz y_poly_parkplatz
