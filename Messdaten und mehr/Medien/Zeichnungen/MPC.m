set(0,'defaulttextinterpreter','latex')
set(groot, 'defaultAxesTickLabelInterpreter','latex')
set(groot, 'defaultLegendInterpreter','latex')
figure

plot(soll(:,1),soll(:,2), 'b-x', 'LineWidth', 1.5)
hold on
plot(ist(1:3,1),ist(1:3,2), 'r-x', 'LineWidth', 1.5)
plot(ist(3:end,1),ist(3:end,2), '-x', 'LineWidth', 1.5)
plot(in(1:5,1), in(1:5,2), 'LineWidth', 1.5)
plot(in(5:end,1), in(5:end,2), 'LineWidth', 1.5)
yticklabels({''})
xticks([-2 -1 0 1 2 3 4 5])
xticklabels({'k-2', 'k-1', 'k', 'k+1', 'k+2', '...', '...', 'k+n'})
legend('Soll-Bahn', 'Ist-Bahn', 'Vorhergesagte Ist-Bahn', 'Vergangene Eingangsgr\"o\ss{}en', 'Zuk\"unftige Eingangsgr\"o\ss{}en')
